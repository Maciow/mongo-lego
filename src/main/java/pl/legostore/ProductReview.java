package pl.legostore;

import org.springframework.data.mongodb.core.index.TextIndexed;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ProductReview {
    @TextIndexed
    private String userName;
    private int rating;
}
