package pl.legostore;

public enum LegoSetDifficulty {
    EASY, MEDIUM, HARD
}
