package pl.legostore;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Getter;

@Document(collection = "legosets")
@Getter
public class LegoSet {

    @Id
    private String id;

    @TextIndexed
    private String name;

    private LegoSetDifficulty difficulty;

    @Indexed(direction = IndexDirection.ASCENDING)
    @TextIndexed
    private String theme;

    private Collection<ProductReview> reviews = new ArrayList<>();

    @Field("deliveryInfo")
    private DeliveryInfo deliveryInfo;

    @DBRef
    private PaymentOption paymentOption;

    @Transient
    private int nbParts;

    public LegoSet(String name, String theme, LegoSetDifficulty difficulty, DeliveryInfo deliveryInfo, Collection<ProductReview> reviews, PaymentOption paymentOption) {
        this.name = name;
        this.difficulty = difficulty;
        this.theme = theme;
        this.paymentOption = paymentOption;
        if (reviews != null) {
            this.reviews = reviews;
        }
        this.deliveryInfo = deliveryInfo;
    }
}
