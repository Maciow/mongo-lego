package pl.legostore;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import pl.legostore.persistance.ReportService;

@RestController
@RequestMapping("/legostore/api/reports")
@RequiredArgsConstructor
class ReportController {

    private final ReportService reportService;

    @GetMapping("/avgRatingReport")
    public List<AvgRatingModel> avgRatingReport() {
        return reportService.receiveAvgRatingReport();
    }

}
