package pl.legostore.persistance;

import static java.util.Arrays.asList;

import java.time.LocalDate;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.legostore.*;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "app.runner", havingValue = "true")
class DbSeeder implements CommandLineRunner {

    private final LegoSetRepository legoSetRepository;
    private final MongoTemplate mongoTemplate;

    @Override
    public void run(String... args) throws Exception {
        legoSetRepository.deleteAll();
        log.info("Delete all from db");
        mongoTemplate.dropCollection("paymentOption");
        log.info("Drop paymentOption collection");

        PaymentOption creditCardPayment = new PaymentOption(PaymentType.CREDIT_CARD, 0);
        PaymentOption payPallPayment = new PaymentOption(PaymentType.PAY_PAL, 1);
        PaymentOption cashPayment = new PaymentOption(PaymentType.CASH, 10);

        mongoTemplate.insertAll(List.of(creditCardPayment, payPallPayment, cashPayment));

        LegoSet milleniumFalcon = new LegoSet(
            "Millenium Falcon",
            "Star Wars",
            LegoSetDifficulty.HARD,
            new DeliveryInfo(LocalDate.now().plusDays(1), 30, true),
            asList(
                new ProductReview("Dan", 7),
                new ProductReview("Ann", 10),
                new ProductReview("John", 8)
            ),
            creditCardPayment
        );

        LegoSet legoPolice = new LegoSet(
            "Sky Police Air Base",
            "City",
            LegoSetDifficulty.MEDIUM,
            new DeliveryInfo(LocalDate.now().plusDays(3), 50, true),
            asList(
                new ProductReview("Dan", 5),
                new ProductReview("Andrew", 8)
            ),
            creditCardPayment
        );

        LegoSet mcLarenSenna = new LegoSet(
            "McLaren Senna",
            "Speed Champions",
            LegoSetDifficulty.EASY,
            new DeliveryInfo(LocalDate.now().plusDays(7), 70, false),
            asList(
                new ProductReview("Bogdan", 9),
                new ProductReview("Christa", 9)
            ),
            payPallPayment
        );

        LegoSet mindstormEve = new LegoSet(
            "MINDSTORMS EV3",
            "Mindstorms",
            LegoSetDifficulty.HARD,
            new DeliveryInfo(LocalDate.now().plusDays(10), 100, false),
            asList(
                new ProductReview("Cosmin", 10),
                new ProductReview("Jane", 9),
                new ProductReview("James", 10)
            ),
            cashPayment
        );

        List<LegoSet> legoSets = legoSetRepository.saveAll(asList(milleniumFalcon, legoPolice, mcLarenSenna, mindstormEve));
        log.info(String.format("%d leggo sets have been saved", legoSets.size()));
    }
}
