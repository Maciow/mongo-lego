package pl.legostore.persistance;

import java.util.Collection;

import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import pl.legostore.LegoSet;
import pl.legostore.LegoSetDifficulty;

@Repository
public interface LegoSetRepository extends MongoRepository<LegoSet, String>, QuerydslPredicateExecutor<LegoSet> {

    Collection<LegoSet> findAllByThemeContaining(String theme);

    Collection<LegoSet> findAllByDifficultyAndNameStartingWith(LegoSetDifficulty difficulty, String name);

    Collection<LegoSet> findAllBy(TextCriteria criteria);

    @Query("{'delivery.deliveryFee' : {$lt : ?0}}")
    Collection<LegoSet> findAllByDeliveryFeeLessThan(int price);

    @Query("{'reviews.rating' : {$eq : ?0}}")
    Collection<LegoSet> findAllByRating(int rating);

    @Query("{'paymentOption.id' : ?0}")
    Collection<LegoSet> findAllByPaymentOptionId(String id);
}
