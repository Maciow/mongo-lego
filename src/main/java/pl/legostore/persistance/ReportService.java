package pl.legostore.persistance;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.List;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import pl.legostore.AvgRatingModel;
import pl.legostore.LegoSet;

@Service
@RequiredArgsConstructor
public class ReportService {

    private final MongoTemplate mongoTemplate;

    public List<AvgRatingModel> receiveAvgRatingReport() {
        ProjectionOperation projectToMapModel = project()
            .andExpression("name").as("productName")
            .andExpression("{$avg : '$reviews.rating'}").as("avgRating");

        Aggregation avgRatingAggregation = Aggregation.newAggregation(LegoSet.class, projectToMapModel);
        return mongoTemplate
            .aggregate(avgRatingAggregation, LegoSet.class, AvgRatingModel.class)
            .getMappedResults();
    }
}
