package pl.legostore.persistance;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import lombok.extern.slf4j.Slf4j;
import pl.legostore.LegoSet;

@ChangeLog(order = "001")
@Slf4j
public class DataMigrations {

    @ChangeSet(order = "001", author = "Maciej", id = "update nb parts")
    public void updateNbParts(MongoTemplate mongoTemplate){
        Criteria priceZeroCriteria = new Criteria().orOperator(
            Criteria.where("nbParts").is(0),
            Criteria.where("nbParts").is(null));
        mongoTemplate.updateMulti(
            new Query(priceZeroCriteria),
            Update.update("nbParts", 122), LegoSet.class);
        log.info("Applied changeset 001");
    }

    @ChangeSet(order = "002", author = "Maciej", id = "increase nb parts")
    public void increaseNbParts(MongoTemplate mongoTemplate){
        Criteria priceZeroCriteria = new Criteria().orOperator(
            Criteria.where("nbParts").is(122),
            Criteria.where("nbParts").is(null));
        mongoTemplate.updateMulti(
            new Query(priceZeroCriteria),
            Update.update("nbParts", 500), LegoSet.class);
        log.info("Applied changeset 002");
    }

    @ChangeSet(order = "003", author = "Maciej", id = "decrease-nb-parts")
    public void decreaseNbParts(MongoTemplate mongoTemplate){
        Criteria priceZeroCriteria = new Criteria().orOperator(
            Criteria.where("nbParts").is(500),
            Criteria.where("nbParts").is(null));
        mongoTemplate.updateMulti(
            new Query(priceZeroCriteria),
            Update.update("nbParts", 20), LegoSet.class);
        log.info("Applied changeset 003");
    }
}
