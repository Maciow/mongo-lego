package pl.legostore;

import java.util.Collection;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.querydsl.core.types.dsl.BooleanExpression;

import lombok.RequiredArgsConstructor;
import pl.legostore.persistance.LegoSetRepository;

@RestController
@RequiredArgsConstructor
@RequestMapping("legostore/api")
class LegoStoreController {

    private final LegoSetRepository legoSetRepository;

    @PostMapping
    private void insert(@RequestBody LegoSet legoSet) {
        legoSetRepository.insert(legoSet);
    }

    @PutMapping
    private void update(@RequestBody LegoSet legoSet) {
        legoSetRepository.save(legoSet);
    }

    @DeleteMapping("/{id}")
    private void delete(@PathVariable String id) {
        legoSetRepository.deleteById(id);
    }

    @GetMapping
    private Collection<LegoSet> findAll() {
        Sort ascendingOrder = Sort.by("theme").ascending();
        return legoSetRepository.findAll(ascendingOrder);
    }

    @GetMapping("/{id}")
    private ResponseEntity<LegoSet> findById(@PathVariable String id) {
       return legoSetRepository.findById(id)
           .map(ResponseEntity::ok)
           .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping("/theme/{theme}")
    private Collection<LegoSet> findByTheme(@PathVariable String theme) {
        return legoSetRepository.findAllByThemeContaining(theme);
    }

    @GetMapping("/difficulty/{difficulty}/name/{name}")
    private Collection<LegoSet> handLegoSetStaringOnM(@PathVariable String difficulty, @PathVariable String name) {
        return legoSetRepository.findAllByDifficultyAndNameStartingWith(LegoSetDifficulty.valueOf(difficulty.toUpperCase()), name);
    }

    @GetMapping("/fees")
    private Collection<LegoSet> findByFeeLessThan(@RequestParam int feeLessThan) {
        return legoSetRepository.findAllByDeliveryFeeLessThan(feeLessThan);
    }

    @GetMapping("/reviews")
    private Collection<LegoSet> findByGreatReviews(@RequestParam int rate) {
        return legoSetRepository.findAllByRating(rate);
    }

    @GetMapping("/best")
    private Collection<LegoSet> findBestBuys() {
        QLegoSet legoSet = new QLegoSet("doesntmatter");
        BooleanExpression inStockFilter = legoSet.deliveryInfo.inStock.isTrue();
        BooleanExpression smallDeliveryFilter = legoSet.deliveryInfo.deliveryFee.lt(50);
        BooleanExpression greatReviewsFilter = legoSet.reviews.any().rating.eq(10);

        BooleanExpression bestBuysFilter = inStockFilter.and(smallDeliveryFilter).and(greatReviewsFilter);

        return (Collection<LegoSet>) legoSetRepository.findAll(bestBuysFilter);
    }

    @GetMapping("/search/{text}")
    private Collection<LegoSet> fullTextSearch(@PathVariable String text) {
        return legoSetRepository.findAllBy(TextCriteria.forDefaultLanguage().matching(text));
    }

    @GetMapping("/payments/{id}")
    private Collection<LegoSet> findByPaymentOptionId(@PathVariable String id) {
        return legoSetRepository.findAllByPaymentOptionId(id);
    }
}
