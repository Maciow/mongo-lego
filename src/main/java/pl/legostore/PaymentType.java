package pl.legostore;

public enum PaymentType {
    CREDIT_CARD, PAY_PAL, CASH
}
