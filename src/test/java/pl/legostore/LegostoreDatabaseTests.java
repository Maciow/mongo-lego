package pl.legostore;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.RequiredArgsConstructor;
import pl.legostore.persistance.LegoSetRepository;

@RunWith(SpringRunner.class)
@DataMongoTest
public class LegostoreDatabaseTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private LegoSetRepository legoSetRepository;

    @Before
    public void before() {
        legoSetRepository.deleteAll();

        LegoSet milleniumFalcon = new LegoSet(
            "Millenium Falcon",
            "Star Wars",
            LegoSetDifficulty.HARD,
            new DeliveryInfo(LocalDate.now().plusDays(1), 30, true),
            asList(
                new ProductReview("Dan", 7),
                new ProductReview("Ann", 10),
                new ProductReview("John", 8)
            ),
            new PaymentOption(PaymentType.CREDIT_CARD, 0)
        );

        LegoSet legoPolice = new LegoSet(
            "Sky Police Air Base",
            "City",
            LegoSetDifficulty.MEDIUM,
            new DeliveryInfo(LocalDate.now().plusDays(3), 50, true),
            asList(
                new ProductReview("Dan", 5),
                new ProductReview("Andrew", 8)
            ),
            new PaymentOption(PaymentType.CREDIT_CARD, 0)
        );

        legoSetRepository.insert(List.of(milleniumFalcon,legoPolice));
    }

    @Test
    public void findAllByRating_should_return_that_have_a_review_with_rating_of_ten() {
        List<LegoSet> results = (List<LegoSet>) legoSetRepository.findAllByRating(10);
        assertEquals(1, results.size());
        assertEquals("Millenium Falcon", results.get(0).getName());
    }

}
